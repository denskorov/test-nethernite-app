import { createStore } from "vuex";
import { get } from "@/api/packages";
import { debounce } from "@/utils/debounce";

export default createStore({
  state: {
    packages: [],
    activePackage: null,
    search: "",
    paginate: {
      page: 1,
      limit: 10,
    },
  },
  getters: {},
  mutations: {
    SET_PACKAGES: (state, packages) => {
      state.packages = packages;
    },
    INC_PAGE: (state) => {
      state.paginate.page = state.paginate.page + 1;
    },
    DEC_PAGE: (state) => {
      state.paginate.page = state.paginate.page - 1;
    },
    SET_ACTIVE_PACKAGE: (state, p) => {
      state.activePackage = p;
    },
    SET_SEARCH: (state, search) => {
      state.search = search;
    },
  },
  actions: {
    async debounceGetPackages({ dispatch }, { text, cb }) {
      debounce(async (text) => {
        const packages = await dispatch("getPackages", text);
        cb(packages);
      }, 500)(text);
    },
    async getPackages({ state, commit }) {
      if (state.search) {
        const { objects: packages } = await get(
          state.search,
          state.paginate.page
        );
        commit("SET_PACKAGES", packages);
      } else commit("SET_PACKAGES", []);
    },
    async getNextPage({ commit, dispatch }, cb) {
      commit("INC_PAGE");
      await dispatch("getPackages");
      cb();
    },
    async getPrevPage({ commit, dispatch }, cb) {
      commit("DEC_PAGE");
      await dispatch("getPackages");
      cb();
    },
  },
});
