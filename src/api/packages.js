// eslint-disable-next-line no-unused-vars
export const get = async (text, page = 1, offset = 10) => {
  const res = await fetch(
    `https://registry.npmjs.com/-/v1/search?text=${text}&size=${offset}&from=${
      (page - 1) * offset
    }`
  );
  return await res.json();
};
